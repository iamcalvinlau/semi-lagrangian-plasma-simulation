subroutine fsolve(e,b,cur)

  use param , only : m, E0, B0, gm, gp

  implicit none

  real(8), intent(inout) :: e(:,:), b(:,:)
  real(8), intent(in) :: cur(:,:)

  integer :: i  
  real(8) :: fp(1:m), fm(1:m), fmp(1:m), fpp(1:m)


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!  time integration for Ey, Bz (using Jy)  !!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!!!!!!!! define old f (f_p) by Ey & Bz !!!!!!!!!
  fpp(1:m-1) = e(1:m-1,2) + b(1:m-1,3)
  fmp(1:m-1) = e(1:m-1,2) - b(1:m-1,3)
    
!!!!!!!!!!!!!!!!!! get new F_ !!!!!!!!!!!!!!!!!!
!$omp parallel do private(i)
  do i=1,m-1
     fp(i) = fpp(gm(i)) - 0.5*(cur(gm(i),2) + cur(i,2))
     fm(i) = fmp(gp(i)) - 0.5*(cur(gp(i),2) + cur(i,2))

     e(i,2) = 0.5*(fp(i) + fm(i))
     b(i,3) = 0.5*(fp(i) - fm(i))
  end do  
!$omp end parallel do

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!  time integration for Ez, By (using Jz)  !!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!!!!!!!! define old F (F_p) by Ez & By !!!!!!!!!
  fpp(1:m-1) = e(1:m-1,3) - b(1:m-1,2)
  fmp(1:m-1) = e(1:m-1,3) + b(1:m-1,2)
  
!!!!!!!!!!!!!!!!!! get new F_ !!!!!!!!!!!!!!!!!!  
!$omp parallel do private(i)
  do i=1,m-1
     fp(i) = fpp(gm(i)) - 0.5*(cur(gm(i),3) + cur(i,3))
     fm(i) = fmp(gp(i)) - 0.5*(cur(gp(i),3) + cur(i,3))

     e(i,3) = 0.5*( fp(i) + fm(i))
     b(i,2) = 0.5*(-fp(i) + fm(i))
  end do
!$omp end parallel do

  write(1022,'(9e15.7)')e(m/60,1),e(m/60,2),e(m/60,3),b(m/60,1),b(m/60,2),b(m/60,3),cur(m/60,1),cur(m/60,2),cur(m/60,3)

end subroutine fsolve
