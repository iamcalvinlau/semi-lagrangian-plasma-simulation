module param
  
  implicit none  


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!                  setting parameters              !!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  real(8) :: tstep,itstep,Rlength,Rstep,vRlength,vRstep
  real(8), allocatable :: bulk_velocity(:),thermal_velocity(:),charge(:),mass_ratios(:)
  integer :: wave_numbers,particles_per_grid,monitor_e,monitor_h,display_t,icase,irun,&
             num_of_species

  real(8) :: b0(1:3),e0(1:3),dt,ss,ts,sigma,pulse_length
  real(8), allocatable :: ub(:),uth(:),q(:),mass(:)

  integer :: wavenum,ppg,monse,mone,monsh,monh,dispst,dispt,casenum,runnum,&
             nspecies
  logical :: file_exist

  namelist /input_parameters/ bulk_velocity,thermal_velocity,charge,mass_ratios,&
            tstep,itsteps,Rlength,Rsteps,vRlength,vRsteps,&
            monitor_e,monitor_h,display_t,irun

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  !! grid size
  real(8) :: dR,dZ,dP,dvR,dvZ,dvP
  
  !! total # of grid points
  integer :: iR,iZ,iP,jvR,jvZ,jvP    

  !! time iteration
  integer :: time

  real(8) :: gam0, cc, econ, pi, dl
  real(8), allocatable :: vb(:),gamb(:),vth(:),gamth(:)
  integer :: i
  integer, allocatable :: gp(:),g0(:),gm(:)

  contains

    subroutine initial_cal

      nspecies=2 !default for older pwfa.in parameter lists

      inquire(file='cipsl.in',exist=file_exist)
      if (file_exist) then
          open(55,file='cipsl.in',status='old')
          read(55,nml=number_of_species)
          close(55)
      else
          write(*,*)'Cannot find file gtc.in !!!'
          stop
      endif

      nspecies=num_of_species

      allocate(bulk_velocity(1:nspecies),thermal_velocity(1:nspecies),&
               charge(1:nspecies),mass_ratios(1:nspecies))
      allocate(ub(1:nspecies),uth(1:nspecies),q(1:nspecies),mass(1:nspecies))
      allocate(vb(1:nspecies),gamb(1:nspecies),vth(1:nspecies),gamth(1:nspecies))

      inquire(file='pwfa.in',exist=file_exist)
      if (file_exist) then
          open(55,file='pwfa.in',status='old')
          read(55,nml=input_parameters)
          close(55)
      else
          write(*,*)'Cannot find file gtc.in !!!'
          stop
      endif

      runnum = irun 
      casenum = icase
      b0(1:3) = (/ 0.0d0, 0.0d0, field_amplitude /)
      e0(1:3) = (/ 0.0d0, field_amplitude,0.0d0 /)
      wavenum = wave_numbers
      pulselength = 0.5*pulse_length

      do i=1,nspecies
          ub(i) = bulk_velocity(i)
          uth(i) = thermal_velocity(i)
          q(i) = charge(i)
          mass(i) = mass_ratios(i)
      enddo

      dt = step_size
      ts = sim_duration

      ssR = Rlength
      iR = Rsteps
      dR = ssR/iR
      vsR = vRlength
      jvR = vRsteps
      dvR = vsR/jvR

      monse = monitor_e
      mone = monitor_e
      monsh = monitor_h
      monh = monitor_h
      dispst = display_t
      dispt = display_t

      pi = 4.0*atan(1.0)
      cc = dt/(2.0*sigma*ppg)
      econ = sigma*ppg

      do i=1,nspecies
          gamb(i) = sqrt(1.0 + ub(i)**2)
          vb(i) = ub(i)/gamb(i)
          gamth(i) = sqrt(1.0 + uth(i)**2)
          vth(i) = uth(i)/gamth(i)
      enddo
 
      !! electron species assumed to be 2 
      gam0 = gamb(2)
      dl = sqrt(2.0*(gamth(2)-1.0))*sigma/dx

      time = nint(ts/dt)

      monse = int(real(time)/real(mone))
      if(monse==0) monse=1
      monsh = int(real(time)/real(monh))
      if(monsh==0) monsh=1
      dispst = int(real(time)/real(dispt))
      if(dispst==0) dispst=1

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!set up periodic grid boundaries!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      allocate(gp(0:iR+1))
      allocate(g0(0:iR+1))
      allocate(gm(0:iR+1))

      do i=0,iR+1
         gp(i) = i+1
         g0(i) = i
         gm(i) = i-1
         if(gp(i)>=iR) gp(i) = gp(i)-iR+1
         if(g0(i)>=iR) g0(i) = g0(i)-iR+1
         if(g0(i)<=0) g0(i) = g0(i)+iR-1
         if(gm(i)<=0) gm(i) = gm(i)+iR-1
      end do
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    end subroutine initial_cal

end module param
