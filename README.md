# README #

This code is written by Calvin Lau of the University of California, Irvine, 2014. At the moment, I'm writing it all in iJulia, but it should be weaned off of that if I get it running without crashing. :)

### Packages needed ###

PyPlot necessary for plotting (necessary for my quick-check test anyway), and HDF5 necessary for writing data.

* Pkg.add("PyPlot")
* Pkg.add("HDF5")

### What is this repository for? ###

* This is an attempt to make a Semi-Lagrangian code for plasma simulations and to learn more about the Julia language. It is quite bare-bones right now, and it currently being written and tested with the iPython notebook extension for Julia.