subroutine history(x,u,e,b,cur,t,pden,nden,pondf)
  
  use param , only : m, dx, dt, n0, runnum, time, q, nspecies
  
  implicit none
  
  integer, intent(in) :: t
  real(8), intent(in) :: pden(:), nden(:)
  real(8), intent(in) :: e(:,:), b(:,:), cur(:,:), pondf(:,:)
  real(8), intent(in) :: x(:,:,:), u(:,:,:)
  
  real(8) :: gamt(1:2)
  integer :: i, k, j, s
  character :: filen*7
  

  
  write(filen,'(i7.7)') t+(runnum*time)
  open(30, file='f'//filen//'.dat')
  open(33, file='p'//filen//'.dat')
  open(34, file='n'//filen//'.dat')
  if(nspecies==3)then
     open(35, file='s3_'//filen//'.dat')
  elseif(nspecies==4)then
     open(35, file='s3_'//filen//'.dat')
     open(36, file='s4_'//filen//'.dat')
  endif

!!! calc ponderomotive force: q(vy) cross Bz !!!

  do i=1,m-1
!!! grid information !!!
     write(30,'(11e15.7)') real(i)*dx,e(i,1),e(i,2),e(i,3),b(i,1),b(i,2),b(i,3),cur(i,2),cur(i,3),pden(i),nden(i) 
  end do


!!! particles in phase space !!!
  do s=1,nspecies
     j=32+s
     do k=1,n0
        gamt(s) = sqrt(1.0 + dot_product(u(k,1:3,s),u(k,1:3,s)))
        write(j,'(6e15.7)') x(k,1,s)*dx, u(k,1,s), u(k,2,s), u(k,3,s), gamt(s), pondf(k,s)
     enddo
  enddo

  
  close(30)
  close(33)
  close(34)
  if(nspecies==3)then
     close(35)
  elseif(nspecies==4)then
     close(35)
     close(36)
  endif 
 
end subroutine history
